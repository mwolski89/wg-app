const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('routes.json')
const middlewares = jsonServer.defaults()
const fs = require("fs");
const path = require('path');

server.use(function(req, res, next){
  setTimeout(next, 1000);
});

server.use(middlewares);

/**
 * Get api responsens from api folder.
 */
server.get('/*', (req, res) => {
  const fileUrl = path.dirname(__filename) + "/api" + req.url + ".json";

  console.log(req.url);
  const contents = fs.readFileSync(fileUrl);
  res.status(200).jsonp(JSON.parse(contents));
});

server.post('/*', (req, res) => {
  console.log('request: ');
  console.log(req.url);

  res.status(200).jsonp();
});

server.delete('/*', (req, res) => {
  console.log('request: ');
  console.log(req.url);

  res.status(200).jsonp();
});

server.put('/*', (req, res) => {
  console.log('request: ');
  console.log(req.url);

  res.status(200).jsonp();
});

server.use(router)

server.listen(8080, () => {
  console.log('JSON Server is running')
});
