#WG Cost App

## INSTALLATION

``` bash
npm install
```

I am using json-server for mocking the backend. To run the mock perform
```bash
npm run mock
```

To start the frontend perform
```bash
npm start
```



# Doku

Ich habe mich entschieden die UI Komponenten zu bauen, da ich aktuell in der Thematik drin bin und Spass daran habe.

## Planung
Folgende Dinge wurden vor der Entwicklungsphase gemacht:
* Ich werde das Projekt etwas umbauen, da ich es nicht gut finde, dass Backend und Frontend in einem Modul drin sind. Das Frontend baue ich in einem seperatem Modul.
* NPM weisst darauf hin, dass einige veraltete Versionen der Abhaengigkeiten verwendet werden und Luecken aufweisen:
```
added 1131 packages from 1162 contributors and audited 7845 packages in 67.879s
found 122 vulnerabilities (23 low, 78 moderate, 21 high)
```
* Ich bin fit in React, dacher habe ich ein neues Projekt erstellt und baue die App mit ReactJS.
* Fuer UI Komponenten verwende ich Material UI fuer React (ist fuer mich neu und in der Uebung sehe ich es als Chance es auszuprobieren)
* Da ich kein Backend habe, verwende ich die app json-server, die mir anhand von Requests statische Responses zuruekschickt.
* Ich habe mir eine kleine Skizze gemacht, wie ich mir die App vorstelle und werde daraus Komponenten fuer React definieren.


## Resultat
Folgende Features wurden implementiert:
* Benutzer der Apps können Einkäufe (Name des Einkäufers, Name des Produktes, Datum, Preis) abspeichern
* Im Frontend wird das Einkaufsprotokoll (also die Liste aller erfassten Einkäufe) angezeigt
* Gespeicherte Einkäufe können (einzeln) gelöscht werden.
* Beim Einkaufsprotokoll wird die Summe der Preise aller getätigten Einkäufe angezeigt.
* Gespeicherte Einkäufe können (einzeln) bearbeitet werden.


## Verbesserungsvorschlaege
* Aufgrund des Zeitmangels kam ich nicht dazu Unit Tests zu schreiben. Auch bin ich mir nicht sicher, ob die Anwendung auf Safari oder dem Internet Explorer funktioniert. Dazu koennte man UI Tests mit Browserstack realisieren.
* Um in Zukunft effizienter zu entwickeln koennte man sich gedanken machen, wie Daten Modelle zwischen Backend und Frontend besser sycnhron gehalten werden. Beispielsweise koennte man mit Swagger/OpenAPI Services und Datenmodelle generieren lassen.
* Ein weiterer wichtiger Punkt ist die Validierung der Daten. Viele Validierungsfehler koennte man bereits im Frontend zeigen, bevor sie vom Backend erkannt werden. Auch hier koennte man eine sinnvolle Loesung finden, um Validierungen vom Backend und Frontend synchron zu halte. Zum Beispiel durch das Pflegen einer Konfigurationsdatei, die Beschreibungen enthaelt, welche Werte fuer welchen Service Optional sind oder gesetzt sein muessen.
* Uebersetzungen einfuehren!
* Login entwickeln
* Docker Container fuer End-To-End Testing + Release
* Es gibt potenziall, dass die App auch auf Mobilgeraeten verwendet wird, daher koennte man es auch in mobilen Browsern verwenden. Denke Dazu muss man ein paar kosmetische Anpassungen im index.css machen.

## Kritik
* Ich bin nicht ganz Gluecklich, wie der Code und die Klassen strukturiert sind. Hatte mir nicht genug Zeit genommen, um es perfekt zu machen.
* Einige dinge die sich in Zukufnt raechen werden, sind, wenn man keine Strategie fuer Uebersetzungen hat. Aktuell gibts es nicht viele Texte in der App, aber es waere besser, wenn man rechtzeitig eine Loesung einfuehrt. Ausserdem koennte man die Localization auch dazu verwenden, um die Preise zu formatieren.
* Aktuell ist die Klasse ShoppingList.tsx sehr gross, das kann man sauberer machen!
* Die Klasse backend-api.ts ist sporadisch entwickelt worden und verwendet die Browserbasierte methode fetch(wird lieder nicht bom Internet Explorer unterstuetzt). Die Klasse ist nicht sinnvoll entwicketl. Beispielsweise koennen Requests nicht abgebrochen werden. Bei zwei zeitgleich abgeschickten Requests, die denselben Endpoint ansprechen, koennte es in der aktuellen Loesung passieren, dass ich einen veralteten Response bekomme.
* Ich bin nich Gluecklich mit dem CSS setup. Material UI nutzt ein anderes Pattern um Komponenten kosmetisch anzupassen. Das muesste man sich nochmal anschauen und dabei darauf achten, dass man sich nicht zu sehr abhaengig von Material UI macht.
