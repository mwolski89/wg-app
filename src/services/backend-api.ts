import ShoppingList from "../components/shopping-list/ShoppingList";
import { IShoppingItem } from "../models/IShoppingItem";

namespace BackendApi {

 const getApi = <T>(url: string): Promise<T> => {
    return fetch(url)
      .then(response => {
        if (!response.ok) {
          throw new Error(response.statusText)
        }
        return response.json().then(data => data as T);
      })
  };

  const postApi = <T>(url: string, data?: any): Promise<Response> => {
    return fetch(url, {
      method: 'POST',
      body: data
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(response.statusText)
        }
        return response;
      })
  };

  const putApi = <T>(url: string, data?: any): Promise<Response> => {
    return fetch(url, {
      method: 'PUT',
      body: data
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(response.statusText)
        }
        return response;
      })
  };

  const deleteApi = <T>(url: string, data?: any): Promise<Response> => {
    return fetch(url, {
      method: 'DELETE',
      body: data
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(response.statusText)
        }
        return response;
      })
  };

  export const requestShoppingList = () => {
    return getApi<IShoppingItem[]>('/shopping-list-items');
  };

  export const addShoppingListItem = (data: any) => {
   return postApi<ShoppingList[]>('/shopping-item/add', JSON.stringify(data));
  };

  export const deleteShoppingItem = (itemId: string) => {
   return deleteApi<string>('/shopping-item/delete', itemId);
  };

  export const updateShoppingItem = (data: any) => {
    return putApi<ShoppingList[]>('/shopping-item/update', JSON.stringify(data));
  };
}

export default BackendApi;
