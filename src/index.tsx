import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './assets/index.css';
import './assets/theme.css';

import unregister from './registerServiceWorker';

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);
// registerServiceWorker();
unregister();
