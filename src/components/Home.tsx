import { Paper } from "@material-ui/core";
import * as React from 'react';
import MenuView from "./menu/MenuView";
import ShoppingList from "./shopping-list/ShoppingList";

const Home: React.FunctionComponent<{}> = () => {

  return(
          <>
            <MenuView />
            <Paper
              elevation={1}
              square
            >
              <ShoppingList/>
            </Paper>
        </>
  )
};

export default Home;
