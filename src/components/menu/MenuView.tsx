import { AppBar, Toolbar, Typography } from "@material-ui/core";
import * as React from "react";

const MenuView: React.FunctionComponent<{}> = () => {

  return(
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" color="inherit">
          Home
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default MenuView;
