import { Paper } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import RefreshIcon from '@material-ui/icons/Refresh';
import * as React from "react";

interface  Props {
  errorMessage: string;
  retry?:() => void;
}

const ErrorView: React.FunctionComponent<Props> = (props: Props) => {

  const renderRetry = (retry?: () => void) => {
    if (retry != undefined) {
    return(
      <Button
        variant="contained"
        color="secondary"
        onClick={() => retry()}
      >
        Retry again
        <RefreshIcon />
      </Button>
    );
    }
    return undefined;
  };

  return (
    <div>
      <Paper elevation={1} className="module">
        <Typography variant="h5" component="h3">
          Error
        </Typography>
        <Typography component="p">
          {props.errorMessage}
        </Typography>
        {renderRetry(props.retry)}
      </Paper>
    </div>
  );
};

export default ErrorView;
