import { Typography } from "@material-ui/core";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import * as React from "react";
import { IShoppingItem } from "../../models/IShoppingItem";

interface Props {
  item: IShoppingItem;
  onSelectedItem: (item: IShoppingItem) => void;
}

const ShoppingListItem: React.FunctionComponent<Props> = (props:Props) => {

  const { item, onSelectedItem } = props;

  return(
    <div
      className={item.status}
    >
      <ListItem
      onClick={() => onSelectedItem(item)}
      button
    >
      <ListItemText
        primary={item.title}
        secondary={`Assigned to ${item.createdBy}`  }
      />
      <Typography variant="body1" color="inherit">
        {item.price} CHF
      </Typography>
    </ListItem>
    </div>
  )
};

export default ShoppingListItem;
