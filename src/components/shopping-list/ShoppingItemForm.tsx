import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField
} from "@material-ui/core";
import { Delete } from "@material-ui/icons";
import * as React from "react";
import { IShoppingItem, ShoppingItem } from "../../models/IShoppingItem";
import BackendApi from "../../services/backend-api";
import ShoppingItemUtils from "../../utils/ShoppingItemUtils";

import ErrorView from "../error-view/ErrorView";

export enum ViewMode {
  SHOW,
  CREATE
}

export enum Action {
  NONE,
  CREATE,
  UPDATE,
  DELETE
}

interface Props {
  isOpen: boolean;
  onDone:(shouldReload: boolean) => void;
  mode: ViewMode;
  selectedItem?: IShoppingItem;
}

interface State {
  item: IShoppingItem;
  isLoading: boolean;
  errorMessage?: string;
  peformedAction: Action;
}

class ShoppingItemForm extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      item: props.selectedItem ? props.selectedItem : new ShoppingItem(),
      isLoading: false,
      peformedAction: Action.NONE,
    };
  }

  onClose = () => {
    this.setState({
      isLoading: false,
    });
    this.props.onDone(false);
  };

  performValidatedAction = (action: Action) => {
    if (this.state.isLoading) {
      return;
    }

    if (ShoppingItemUtils.validate(this.state.item)) {
      this.performAction(action);
    } else {
      this.renderError();
    }
  };

  performAction = (action:Action) => {
    switch (action) {
      case Action.CREATE:
        this.createAction();
        break;
      case Action.UPDATE:
        this.updateAction();
        break;
      case Action.DELETE:
        this.deleteAction();
        break
    }
  };

  deleteAction = () => {
    if (this.state.isLoading) {
      return;
    }

    this.setState({
      ...this.state,
      isLoading: true,
    });

    const { item } = this.state;
    BackendApi.deleteShoppingItem(item.id!)
              .then((response) => {
                if (response.ok) {
                  this.props.onDone(true);
                }
              })
              .catch((error) => {
                this.setState({
                  isLoading: false,
                  errorMessage: error.message,
                });
              });
  };

  createAction = () => {
    const { item } = this.state;
    this.setState({
      ...this.state,
      peformedAction: Action.CREATE,
    });

    BackendApi.addShoppingListItem(item)
              .then((response) => {
                if (response.ok) {
                  this.props.onDone(true);
                }
              })
              .catch((error) => {
                this.setState({
                  isLoading: false,
                  errorMessage: error.message,
                });
              });
  };

  updateAction = () => {
    const { item } = this.state;

    BackendApi.updateShoppingItem(item)
              .then((response) => {
                if (response.ok) {
                  this.props.onDone(true);
                }
              })
              .catch((error) => {
                this.setState({
                  isLoading: false,
                  errorMessage: error.message,
                });
              });
  };
  render() {
    return (
      <>
        <form
          id="form-item"
        >
          <Dialog open={this.props.isOpen}
                  onClose={() => this.onClose()}
                  aria-labelledby="form-dialog-title"
          >
            <DialogTitle
              id="form-dialog-title"
            >
              {
                this.props.mode == ViewMode.CREATE ? 'Create new Item' : 'Details'
              }
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                What do you buy next?
              </DialogContentText>
              {(this.props.mode === ViewMode.CREATE) && this.renderCreateTextFields()}
              {(this.props.mode === ViewMode.SHOW) && this.renderUpdateTextFields()}
            </DialogContent>
            {this.renderFormAction()}
            {this.renderError()}
          </Dialog>
        </form>
      </>
    );
  }

  renderFormAction = () => {
    const { mode } = this.props;
    return(
      <DialogActions>
        <Button
          onClick={() => this.onClose()}
          color="default"
        >
          Cancel
        </Button>
        {mode === ViewMode.CREATE && (
          <Button
            form="form-item"
            color="primary"
            onClick={() =>this.performValidatedAction(Action.CREATE)}
          >
            Add
          </Button>
        )}

        {mode === ViewMode.SHOW && (
          <>
            <Button
              form="form-item"
              color="primary"
              onClick={() =>this.performValidatedAction(Action.UPDATE)}
            >
              Update
            </Button>
          <Button
            form="form-item"
            color="secondary"
            onClick={() =>this.performValidatedAction(Action.DELETE)}
          >
            <Delete/>
          </Button>
          </>
        )}

      </DialogActions>
    );
  };

  renderError = () => {
    if (this.state.errorMessage != undefined) {
      return (
        <ErrorView
          errorMessage={this.state.errorMessage!}
        />
      );
    }
    return undefined;
  }

  renderCreateTextFields = () => {
    return (
      <>
        <TextField
          autoFocus
          margin="dense"
          id="title"
          name="title"
          label="title"
          type="text"
          fullWidth
          onChange={e => {
            this.setState({
              item: {
                ...this.state.item,
                title: e.target.value
              }
            })
          }}
          required
        />
        <TextField
          margin="dense"
          id="price"
          name="price"
          label="price"
          type="number"
          fullWidth
          onChange={e => {
            this.setState({
              item: {
                ...this.state.item,
                price: Number(e.target.value),
              }
            })
          }}
          required
        />
      </>
    );
  }

  renderUpdateTextFields = () => {
    const item = this.state.item;

    if (item === undefined) {
      return undefined;
    }

    return (
      <>
        <TextField
          autoFocus
          margin="dense"
          id="title"
          name="title"
          label="title"
          type="text"
          fullWidth
          onChange={e => {
            this.setState({
              item: {
                ...this.state.item,
                title: e.target.value
              }
            })
          }}
          value={item.title}
          required
        />
        <TextField
          margin="dense"
          id="price"
          name="price"
          label="price"
          type="number"
          fullWidth
          onChange={e => {
            this.setState({
              item: {
                ...this.state.item,
                price: Number(e.target.value),
              }
            })
          }}
          value={item.price}
          required
        />
        <TextField
          margin="dense"
          id="assignedTo"
          name="assignedTo"
          label="Assigned To"
          type="text"
          fullWidth
          onChange={e => {
            this.setState({
              item: {
                ...this.state.item,
                assignedTo: e.target.value,
              }
            })
          }}
          value={item.assignedTo}
          required
        />
        <TextField
          margin="dense"
          id="status"
          name="status"
          label="Status"
          type="status"
          fullWidth
          onChange={e => {
            this.setState({
              item: {
                ...this.state.item,
                status: e.target.value,
              }
            })
          }}
          value={item.status}
          required
        />
      </>
    );
  }
}

export default ShoppingItemForm;
