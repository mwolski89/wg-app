import { IconButton } from "@material-ui/core";
import { AddShoppingCart } from "@material-ui/icons";
import * as React from "react";

interface Props {
  onAddItemClicked: () => void;
}

const AddItemButton: React.FunctionComponent<Props> = (props: Props) => {
  return(
    <IconButton
      onClick={() => props.onAddItemClicked()}
      aria-owns={open ? 'menu-appbar' : undefined}
      aria-haspopup="true"
      color="inherit"
    >
      <AddShoppingCart />
    </IconButton>
  );
};

export default AddItemButton;
