import { List, Typography } from "@material-ui/core";
import * as React from 'react';
import { IShoppingItem } from "../../models/IShoppingItem";
import BackendApi from "../../services/backend-api";
import ShoppingItemUtils from "../../utils/ShoppingItemUtils";
import ErrorView from "../error-view/ErrorView";
import LoadingIndicatorView from "../loading-indicator/LoadingIndicatorView";
import ShoppingListItem from "../shopping-list-item/ShoppingListItem";
import AddItemButton from "./AddItemButton";
import ShoppingItemForm, { ViewMode } from "./ShoppingItemForm";

enum Action {
  NONE,
  ADD_ITEM,
  SHOW_ITEM
}

interface State {
  isLoading: boolean;
  shoppingList: IShoppingItem[];
  errorMessage?: string;
  action: Action;
  selectedItem?: IShoppingItem;
}

class ShoppingList extends React.Component<{}, State> {

  constructor(props: any) {
    super(props);

    this.state = {
      isLoading: true,
      shoppingList: [],
      action: Action.NONE,
    };
  }

  componentDidMount(): void {
    this.requestItems();
  }

  render() {
    return(
      <div className="module">
        <Typography variant="h6">
          Shopping List
        </Typography>
        <div>
          {this.rendedTotalAmount()}
          {this.renderList()}
          {this.renderLoading()}
          {this.renderError()}
          {this.renderAddListItem()}
          {this.renderShowItem()}
        </div>
      </div>
    )
  }

  requestItems = () => {
    this.setState({
      isLoading: true,
      shoppingList: [],
      errorMessage: undefined,
      action: Action.NONE,
    });

    BackendApi.requestShoppingList()
              .then((shoppingList) => {
                if (shoppingList.length > 0) {
                  this.setState({
                    isLoading:false,
                    shoppingList,
                  });
                } else {
                  this.setState({
                    isLoading: false,
                    shoppingList: [],
                    errorMessage:'No items',
                  })
                }
              }).catch((error) => {
                this.setState({
                  isLoading: false,
                  errorMessage: error.message,
                })
              });
  };

  renderList = () => {
    const shoppingList = this.state.shoppingList;

    if (shoppingList.length> 0) {
      return(
        <>
          <List>
            {shoppingList.map((item) => {
              return (
                <ShoppingListItem
                  item={item}
                  key={item.id}
                  onSelectedItem={(item) => this.onShowDetails(item)}
                />
                )
            })}
          </List>
          <AddItemButton
            onAddItemClicked={() => {this.onAddItemClicked()}}
          />
        </>
      );
    }
    return undefined;
  };

  renderLoading = () => {
    if (this.state.isLoading) {
      return(
        <LoadingIndicatorView/>
      );
    }
    return undefined;
  };

  renderError = () => {
    if (this.state.errorMessage != undefined) {
      return (
        <ErrorView
          errorMessage={this.state.errorMessage!}
          retry={this.requestItems}
        />
      );
    }
    return undefined;
  };

  renderAddListItem = () => {
    return(
      <>
        {(this.state.action === Action.ADD_ITEM) && (
          <ShoppingItemForm
            isOpen={this.state.action === Action.ADD_ITEM}
            onDone={(shouldReload) => this.clearAction(shouldReload)}
            mode={ViewMode.CREATE}
          />
        )}
      </>
    );
  };

  renderShowItem = () => {
    return(
      <>
        {(this.state.action === Action.SHOW_ITEM) && (
          <ShoppingItemForm
            isOpen={this.state.action === Action.SHOW_ITEM}
            onDone={(shouldReload) => this.clearAction(shouldReload)}
            mode={ViewMode.SHOW}
            selectedItem={this.state.selectedItem}
          />
        )}
      </>
    );
  };

  rendedTotalAmount = () => {
    return(
      <Typography variant={"display1"} component={"h6"}>
        {ShoppingItemUtils.totalAmountDoneRepresentational(this.state.shoppingList)}
      </Typography>
    );
  }

  clearAction = (shouldReload: boolean) => {
    this.setState({
      ...this.state,
      action: Action.NONE,
    });
    if (shouldReload) {
      this.requestItems();
    }
  };

  onAddItemClicked = () => {
    this.setState({
      ...this.state,
      action: Action.ADD_ITEM,
    });
  };

  onShowDetails = (item: IShoppingItem) => {
    this.setState({
      ...this.state,
      action: Action.SHOW_ITEM,
      selectedItem: item,
    });
  };

}

export default ShoppingList
