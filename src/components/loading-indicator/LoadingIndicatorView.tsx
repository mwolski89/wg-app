import CircularProgress from '@material-ui/core/CircularProgress';
import * as React from 'react';

const LoadingIndicatorView: React.FunctionComponent<{}> = () => {
  return(
    <CircularProgress className="module" />
  );
};

export default LoadingIndicatorView;
