export interface IShoppingItem {
  id: string;
  createdBy: string,
  assignedTo: string,
  title: string,
  creationDate: Date;
  price: number;
  status: string
}

export class ShoppingItem implements IShoppingItem{

  constructor() { }

  assignedTo: string;
  createdBy: string;
  creationDate: Date;
  id: string;
  price: number;
  status: string;
  title: string;
};
