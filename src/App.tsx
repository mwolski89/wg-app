import * as React from 'react';
import Home from './components/Home';
import { UserProvider } from './context/User.Context';
import { IUser } from "./models/IUser";

const demoUser: IUser = {
  username: 'Mark',
  id: '1',
  creationTime: new Date(),
  enabled: true,
  modificationTime: new Date(),
};

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <UserProvider user={demoUser}>
          <Home />
        </UserProvider>
      </div>
    );
  }
}

export default App;
