import * as React from 'react';
import { IUser } from "../models/IUser";

interface State {
  user: IUser
}

interface Props {
  user: IUser;
}

const UserContext = React.createContext<State>({
  user: undefined!
});

export class UserProvider extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      user: props.user
    };
  }

  render() {
    return(
      <UserContext.Provider value={this.state}>
        {this.props.children}
      </UserContext.Provider>
    );
  }
}

export const UserConsumer = UserContext.Consumer;


