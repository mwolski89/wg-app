import { IShoppingItem } from "../models/IShoppingItem";

namespace ShoppingItemUtils {

  export const totalAmountDoneRepresentational = (items: IShoppingItem[]): string => {
    const totalAmount = ShoppingItemUtils.totalAmountDone(items);
    return `Total spend: ${totalAmount} CHF`;
  };

  export const totalAmountDone = (items: IShoppingItem[]): number => {
    let result = 0;
    items.map((item) => {
      if (item.status === 'done') {
        result += item.price
      }
    });
    return result;
  };

  export const validate = (item: IShoppingItem): boolean => {
    // TODO: validate item!
    return true;
  };
}


export default ShoppingItemUtils;
